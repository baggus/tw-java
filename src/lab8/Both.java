package lab8;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Aleksander Ciepiela on 2015-05-15.
 */
public class Both {

    private static final int N = Philosopher.N;

    public static void main(String[] args) {

        int numberOfPhilosophers = 5;
        Fork[] forks = new Fork[numberOfPhilosophers];

        for (int i = 0; i < numberOfPhilosophers; i++) {
            forks[i] = new Fork(false, i);
        }

        final CountDownLatch startLatch = new CountDownLatch(1);
        final CountDownLatch finishLatch = new CountDownLatch(numberOfPhilosophers);
        final Object synchronizer = new Object();
        Worker[] philosophers = new Worker[numberOfPhilosophers];

        for (int i = 0; i < numberOfPhilosophers; i++) {
            philosophers[i] = new Worker(startLatch, finishLatch, synchronizer, forks[i % numberOfPhilosophers],
                    forks[(i + 1) % numberOfPhilosophers]);
            philosophers[i].start();
        }

        long start = System.currentTimeMillis();
        startLatch.countDown();
        try {
            finishLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("Took: " + (end - start));


    }

    private static class Fork {

        private final int number;
        private boolean beingUsed;

        private Fork(boolean beingUsed, int number) {
            this.beingUsed = beingUsed;
            this.number = number;
        }

        @Override
        public String toString() {
            return "Fork{" +
                    "number=" + number +
                    '}';
        }
    }

    private static class Worker extends Thread {

        private final CountDownLatch startLatch;
        private final CountDownLatch finalLatch;
        private final Object synchronizer;
        private final Fork leftFork;
        private final Fork rightFork;


        private Worker(CountDownLatch startLatch, CountDownLatch finalLatch, Object synchronizer, Fork leftFork, Fork rightFork) {
            this.startLatch = startLatch;
            this.finalLatch = finalLatch;
            this.synchronizer = synchronizer;
            this.leftFork = leftFork;
            this.rightFork = rightFork;
        }

        @Override
        public void run() {

            try {
                startLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            int i = 0;
            while (i < N) {
                boolean canEat = false;
                synchronized (synchronizer) {
                    if (!leftFork.beingUsed && !rightFork.beingUsed) {
                        leftFork.beingUsed = rightFork.beingUsed = true;
                        canEat = true;
                    }
                }
                if (canEat) {
                    i++;
                    try {
                        System.out.println("Thread " + Thread.currentThread().getName() + " is eating with forks: " + leftFork + " " + rightFork);
                        Thread.sleep(Philosopher.EATING_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    leftFork.beingUsed = rightFork.beingUsed = false;
                    try {
                        System.out.println("Thread " + Thread.currentThread().getName() + " is thinking");
                        Thread.sleep(Philosopher.THINKING_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            finalLatch.countDown();
            System.out.println("-----Thread " + Thread.currentThread().getName() + " finished");
        }
    }


}
