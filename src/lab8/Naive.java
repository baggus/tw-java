package lab8;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Aleksander Ciepiela on 2015-05-11.
 */
public class Naive {

    private static int N = Philosopher.N;

    public static void main(String[] args) throws InterruptedException {


        final int numOfPhilosophers = 5;

        Lock[] forks = new Lock[numOfPhilosophers];
        for (int i = 0; i < numOfPhilosophers; i++) {
            forks[i] = new ReentrantLock();
        }

        Philosopher[] philosophers = new Philosopher[numOfPhilosophers];
        for (int i = 0; i < numOfPhilosophers; i++) {
            philosophers[i] = new Philosopher(forks[i % numOfPhilosophers], forks[(i + 1) % numOfPhilosophers]);
        }


        //naive
        final CountDownLatch naiveCountDownLatch = new CountDownLatch(1);
        final CountDownLatch finishLatch = new CountDownLatch(numOfPhilosophers);
        for (int i = 0; i < numOfPhilosophers; i++) {
            new NaivePhilosopherWorker(philosophers[i], naiveCountDownLatch, finishLatch).start();
        }

        naiveCountDownLatch.countDown();
        long start = System.currentTimeMillis();
        finishLatch.await();
        long end = System.currentTimeMillis();
        System.out.println("Took: " + (end - start));


    }

    private static class NaivePhilosopherWorker extends Thread {

        private final Philosopher philosopher;
        private final CountDownLatch latch;
        private final CountDownLatch finishLatch;

        public NaivePhilosopherWorker(Philosopher philosopher, CountDownLatch latch, CountDownLatch finishLatch) {
            this.philosopher = philosopher;
            this.latch = latch;
            this.finishLatch = finishLatch;
        }


        @Override
        public void run() {
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < N; i++) {
                philosopher.leftFork.lock();
                philosopher.rightFork.lock();
                philosopher.eat();
                philosopher.rightFork.unlock();
                philosopher.leftFork.unlock();
                philosopher.think();
            }
            finishLatch.countDown();
        }
    }


}
