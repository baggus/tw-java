package lab8;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Aleksander Ciepiela on 2015-05-15.
 */
public class Waiter {


    private static int N = Philosopher.N;

    public static void main(String[] args) throws InterruptedException {


        final int numOfPhilosophers = 5;

        Lock[] forks = new Lock[numOfPhilosophers];
        for (int i = 0; i < numOfPhilosophers; i++) {
            forks[i] = new ReentrantLock();
        }

        Philosopher[] philosophers = new Philosopher[numOfPhilosophers];
        for (int i = 0; i < numOfPhilosophers; i++) {
            philosophers[i] = new Philosopher(forks[i % numOfPhilosophers], forks[(i + 1) % numOfPhilosophers]);
        }


        final CountDownLatch naiveCountDownLatch = new CountDownLatch(1);
        final CountDownLatch timeLatch = new CountDownLatch(5);
        final Object synchronizer = new Object();
        for (int i = 0; i < numOfPhilosophers; i++) {
            new WaiterPhilosopherWorker(philosophers[i], naiveCountDownLatch, timeLatch, synchronizer).start();
        }

        long start = System.currentTimeMillis();
        naiveCountDownLatch.countDown();
        timeLatch.await();
        long stop = System.currentTimeMillis();
        System.out.println("With waiting took: " + (stop - start));

    }

    private static class WaiterPhilosopherWorker extends Thread {

        private final Philosopher philosopher;
        private final CountDownLatch latch1, latch2;
        private final Object synchronizer;

        public WaiterPhilosopherWorker(Philosopher philosopher, CountDownLatch latch, CountDownLatch latch2, Object synchronizer) {
            this.philosopher = philosopher;
            this.latch1 = latch;
            this.latch2 = latch2;
            this.synchronizer = synchronizer;
        }

        @Override
        public void run() {
            try {
                latch1.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < N; i++) {

                synchronized (synchronizer) {
                    philosopher.leftFork.lock();
                    philosopher.rightFork.lock();
                }
                philosopher.eat();
                philosopher.leftFork.unlock();
                philosopher.rightFork.unlock();
                philosopher.think();
            }
            latch2.countDown();
            System.out.println("-----Thread " + Thread.currentThread().getName() + " finished");
        }
    }


}
