package lab8;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Aleksander Ciepiela on 2015-05-13.
 */
public class Waiting {

    private static int N = Philosopher.N;

    public static void main(String[] args) throws InterruptedException {


        final int numOfPhilosophers = 5;

        Lock[] forks = new Lock[numOfPhilosophers];
        for (int i = 0; i < numOfPhilosophers; i++) {
            forks[i] = new ReentrantLock();
        }

        Philosopher[] philosophers = new Philosopher[numOfPhilosophers];
        for (int i = 0; i < numOfPhilosophers; i++) {
            philosophers[i] = new Philosopher(forks[i % numOfPhilosophers], forks[(i + 1) % numOfPhilosophers]);
        }


        final CountDownLatch naiveCountDownLatch = new CountDownLatch(1);
        final CountDownLatch timeLatch = new CountDownLatch(5);
        for (int i = 0; i < numOfPhilosophers; i++) {
            new WaitingPhilosopherWorker(philosophers[i], naiveCountDownLatch, timeLatch).start();
        }

        long start = System.currentTimeMillis();
        naiveCountDownLatch.countDown();
        timeLatch.await();
        long stop = System.currentTimeMillis();
        System.out.println("With waiting took: " + (stop - start));

    }

    private static class WaitingPhilosopherWorker extends Thread {

        private final Philosopher philosopher;
        private final CountDownLatch latch1, latch2;

        public WaitingPhilosopherWorker(Philosopher philosopher, CountDownLatch latch, CountDownLatch latch2) {
            this.philosopher = philosopher;
            this.latch1 = latch;
            this.latch2 = latch2;
        }

        @Override
        public void run() {

            final Random r = new Random();

            try {
                latch1.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            final int waitingTime = 10;
            int i = 0;
            while (i < N) {
                try {
                    if (philosopher.leftFork.tryLock(waitingTime + r.nextInt(waitingTime), TimeUnit.MILLISECONDS)) {
                        if (philosopher.rightFork.tryLock(waitingTime + r.nextInt(waitingTime), TimeUnit.MILLISECONDS)) {
                            philosopher.eat();
                            philosopher.leftFork.unlock();
                            philosopher.rightFork.unlock();
                            philosopher.think();
                            i++;
                        } else {
                            philosopher.leftFork.unlock();
                            Thread.sleep(waitingTime + r.nextInt(waitingTime));
                        }
                    } else {
                        Thread.sleep(waitingTime + r.nextInt(waitingTime));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            latch2.countDown();
            System.out.println("-----Thread " + Thread.currentThread().getName() + " finished");
        }
    }
}
