package lab8;

import java.util.concurrent.locks.Lock;

/**
 * Created by Aleksander Ciepiela on 2015-05-13.
 */
class Philosopher {

    public static final int THINKING_TIME = 10;
    public static final int EATING_TIME = 10;
    public static final int N = 1000;

    public final Lock leftFork;
    public final Lock rightFork;

    public Philosopher(Lock leftFork, Lock rightFork) {
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    public void think() {
        System.out.println(Thread.currentThread().getId() + " thinking");
        try {
            Thread.sleep(THINKING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void eat() {
        System.out.println(Thread.currentThread().getId() + " eating");
        try {
            Thread.sleep(EATING_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getId() + " finished eating");
    }
}
