package lab3;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Aleksander Ciepiela on 2015-03-23.
 */
public class Test {


    public static void main(String[] args) {

        final int numOfThreads = 3;
        final int numOfObjects = 1000;

        System.out.println("CoarseGrainedSynchronizedList");
        measure(numOfThreads, numOfObjects, new CoarseGrainedSynchronizedLinkedList());
        System.out.println();

        System.out.println("Collections.synchronized(new LinkedList())");
        measure(numOfThreads, numOfObjects, Collections.synchronizedList(new LinkedList<>()));
        System.out.println();

        System.out.println("FineGrainedSynchronizedLinkedList");
        measure(numOfThreads, numOfObjects, new FineGrainedSynchronizedLinkedList());
        System.out.println();


    }

    private static void measure(int numOfThreads, int numOfObjects, List<Object> list) {


        final CountDownLatch latch1 = new CountDownLatch(1);
        final Thread[] workerThreads = new Thread[numOfThreads];

        for (int i = 0; i < numOfThreads; i++) {
            final Object[] objects = new Object[numOfObjects];
            for (int j = 0; j < numOfObjects; j++) {
                objects[j] = new Object();
            }
            Thread t = new WorkerThread(objects, list, latch1);
            workerThreads[i] = t;
            t.start();
        }

        long start = System.currentTimeMillis();
        latch1.countDown();
        for (int i = 0; i < numOfThreads; i++) {
            try {
                workerThreads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long end = System.currentTimeMillis();

        System.out.println("Operations took: " + (end - start) + " ms");

        System.out.println("List size: " + list.size());
    }


    private static class WorkerThread extends Thread {

        private final Object[] objects;
        private final List<Object> list;
        private final CountDownLatch mCountDownLatch;

        private WorkerThread(Object[] objects, List<Object> list, CountDownLatch countDownLatch) {
            this.objects = objects;
            this.list = list;
            this.mCountDownLatch = countDownLatch;
        }


        @Override
        public void run() {
            try {
                mCountDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            final Random random = new Random();
            for (int i = 0; i < objects.length; i++) {
                list.add(objects[i]);
                list.contains(objects[random.nextInt(objects.length)]);
                list.remove(objects[random.nextInt(objects.length)]);
            }
            for (int i = 0; i < objects.length / 2; i++) {
                list.remove(objects[i]);
            }
        }
    }
}
