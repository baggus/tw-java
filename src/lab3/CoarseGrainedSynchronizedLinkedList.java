package lab3;

import java.util.AbstractList;

/**
 * Created by Aleksander Ciepiela on 2015-03-27.
 */
public class CoarseGrainedSynchronizedLinkedList extends AbstractList<Object> {


    private Node first;
    private Node last;


    @Override
    public synchronized boolean add(Object o) {
        Node toInsert = new Node(o);
        if (first == null) {
            first = toInsert;
            last = first;
            return true;
        } else {
            last.setNext(toInsert);
            last = toInsert;
            return true;
        }
    }

    @Override
    public Object get(int index) {
        return null;
    }


    @Override
    public synchronized boolean remove(Object o) {

        Node prev;
        if (first == null) {
            return false;
        } else {
            prev = first;
            if (prev.getObject().equals(o)) {
                first = prev.next;
                return true;
            }
        }

        Node next = prev.next;

        while (next != null) {
            if (next.getObject().equals(o)) {
                prev.next = next.next;
                if (next == last) {
                    last = prev;
                }
                return true;
            } else {
                prev = next;
                next = next.next;
            }
        }
        return false;
    }

    @Override
    public synchronized int size() {
        Node prev;
        prev = first;
        if (first == null) {
            return 0;
        }


        int counter = 1;
        Node next = prev.next;

        while (next != null) {
            prev = next;
            next = next.next;
            counter++;
        }

        return counter;
    }

    @Override
    public synchronized boolean contains(Object o) {

        Node prev;
        if (first == null) {
            return false;
        } else {
            prev = first;
            if (prev.getObject().equals(o)) {
                return true;
            }
        }


        Node next = prev.next;

        while (next != null) {
            if (next.getObject().equals(o)) {
                return true;
            } else {
                next = next.next;
            }
        }
        return false;
    }


    private static class Node {

        private final Object mObject;
        private Node next;

        public void setNext(Node next) {
            this.next = next;
        }

        public Node(Object mObject) {
            this.mObject = mObject;
        }

        public Object getObject() {
            return mObject;
        }

    }


}
