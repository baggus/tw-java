package lab3;

import java.util.AbstractList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class FineGrainedSynchronizedLinkedList extends AbstractList<Object> {

    private Node first;
    private Node last;


    @Override
    public boolean add(Object o) {
        Node toInsert = new Node(o);
        synchronized (this) {
            if (first == null) {
                first = toInsert;
                last = first;
                return true;
            } else {
                last.getLock().lock();
                Node tmp = null;
                try {
                    last.setNext(toInsert);
                    tmp = last;
                    last = toInsert;
                } finally {
                    tmp.getLock().unlock();
                }
            }
        }

        return true;
    }

    @Override
    public Object get(int index) {
        return null;
    }


    @Override
    public boolean remove(Object o) {

        Node prev;
        synchronized (this) {
            if (first == null) {
                return false;
            } else {
                prev = first;
                prev.getLock().lock();
                if (prev.getObject().equals(o)) {
                    first = prev.next;
                    prev.getLock().unlock();
                    return true;
                }
            }
        }

        Node next = prev.next;

        while (next != null) {
            next.getLock().lock();
            if (next.getObject().equals(o)) {
                prev.next = next.next;
                if (next == last) {
                    last = prev;
                }
                prev.getLock().unlock();
                next.getLock().unlock();
                return true;
            } else {
                prev.getLock().unlock();
                prev = next;
                next = next.next;
            }
        }
        prev.getLock().unlock();
        return false;
    }

    @Override
    public int size() {
        Node prev;
        synchronized (this) {
            prev = first;
            if (first == null) {
                return 0;
            } else {
                first.getLock().lock();
            }
        }

        int counter = 1;
        Node next = prev.next;

        while (next != null) {
            next.getLock().lock();
            prev.getLock().unlock();
            prev = next;
            next = next.next;
            counter++;
        }

        prev.getLock().unlock();

        return counter;
    }

    @Override
    public boolean contains(Object o) {

        Node prev;
        synchronized (this) {
            if (first == null) {
                return false;
            } else {
                prev = first;
                prev.getLock().lock();
                if (prev.getObject().equals(o)) {
                    prev.getLock().unlock();
                    return true;
                }
            }
        }

        Node next = prev.next;

        while (next != null) {
            if (next.getObject().equals(o)) {
                prev.getLock().unlock();
                return true;
            } else {
                next.getLock().lock();
                prev.getLock().unlock();
                prev = next;
                next = next.next;
            }
        }
        prev.getLock().unlock();
        return false;
    }


    private static class Node {

        private final Object mObject;
        private final Lock mLock = new ReentrantLock();
        private Node next;

        public void setNext(Node next) {
            this.next = next;
        }

        public Node(Object mObject) {
            this.mObject = mObject;
        }

        public Object getObject() {
            return mObject;
        }

        public Lock getLock() {
            return mLock;
        }
    }


}
