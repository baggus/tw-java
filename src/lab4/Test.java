package lab4;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Aleksander Ciepiela on 2015-03-30.
 */
public class Test {


    public static void main(String[] args) {

        long start;
        int threads = 10;
        System.out.println();


        start = System.currentTimeMillis();
        System.out.println("Default settings");
        test(new Database(), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Load factor = 0.75, threads = " + 1);
        test(new Database(16, 0.75f, 1), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Load factor = 0.75, threads = " + 2);
        test(new Database(16, 0.75f, 2), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Load factor = 0.75, threads = " + threads / 3);
        test(new Database(16, 0.75f, threads / 3), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Load factor = 0.75, threads = " + threads / 2);
        test(new Database(16, 0.75f, threads / 2), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Load factor = 0.75, threads = " + threads);
        test(new Database(16, 0.75f, threads), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Load factor = 0.9, threads = " + threads);
        test(new Database(16, 0.9f, threads), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Load factor = 0.5, threads = " + threads);
        test(new Database(16, 0.5f, threads), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");

        start = System.currentTimeMillis();
        System.out.println("Default settings");
        test(new Database(), threads);
        System.out.println("Time: " + (System.currentTimeMillis() - start) + " ms");


    }


    private static void test(Database db, int threads) {
        final String[] keys = new String[50];
        for (int i = 0; i < 50; i++) {
            keys[i] = String.valueOf(i);
        }

        Worker[] workers = new Worker[threads];
        final CountDownLatch countDownLatch = new CountDownLatch(1);

        for (int i = 0; i < threads; i++) {
            workers[i] = new Worker(db, keys, countDownLatch);
            workers[i].start();
        }

        countDownLatch.countDown();
        for (int i = 0; i < threads; i++) {
            try {
                workers[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    private static class Worker extends Thread {

        private final Database db;
        private final String[] keys;
        private final CountDownLatch mCountDownLatch;


        private Worker(Database db, String[] keys, CountDownLatch mCountDownLatch) {
            this.db = db;
            this.keys = keys;
            this.mCountDownLatch = mCountDownLatch;
        }

        @Override
        public void run() {
            try {
                mCountDownLatch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Random random = new Random();
            for (int i = 0; i < 5_000_000; i++) {
                switch (i % 4) {
                    case 0:
                        db.put(keys[i % keys.length], new Object(), random.nextInt(1000));
                        break;
                    case 1:
                        db.put(String.valueOf(i), new Object(), 0);
                        break;
                    case 2:
                        db.get(keys[i % keys.length]);
                        break;
                    default:
                        db.get(String.valueOf(i));
                        break;
                }
            }
        }
    }
}
