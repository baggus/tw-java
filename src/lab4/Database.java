package lab4;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Aleksander Ciepiela on 2015-03-30.
 */
public class Database {

    private final ConcurrentHashMap<String, Pair> mMap;

    public Database() {
        this.mMap = new ConcurrentHashMap<>(10);
    }

    public Database(int defCapacity, float loadFactor, int threads) {
        this.mMap = new ConcurrentHashMap<>(defCapacity, loadFactor, threads);
    }

    public boolean put(String key, Object obj, int lifetime) {
        if (obj == null) {
            return (mMap.remove(key) != null);
        }
        final Pair p = new Pair();
        p.obj = obj;
        if (lifetime > 0) {
            p.timeStamp = System.currentTimeMillis() + lifetime;// * 1000;
        } else {
            p.timeStamp = 0;
        }
        try {
            mMap.put(key, p);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public Object get(String key) {
        final Pair pair = mMap.get(key);
        if (pair == null) {
            return null;
        }
        if (pair.timeStamp < System.currentTimeMillis() && pair.timeStamp != 0) {
            mMap.remove(key, pair);
            return null;
        }
        return pair.obj;
    }

    private static class Pair {
        private Object obj;
        private long timeStamp;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Pair)) return false;

            Pair pair = (Pair) o;

            if (timeStamp != pair.timeStamp) return false;
            return !(obj != null ? !obj.equals(pair.obj) : pair.obj != null);

        }

        @Override
        public int hashCode() {
            int result = obj != null ? obj.hashCode() : 0;
            result = 31 * result + (int) (timeStamp ^ (timeStamp >>> 32));
            return result;
        }
    }
}
