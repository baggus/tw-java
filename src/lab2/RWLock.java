package lab2;

/**
 * Created by Aleksander Ciepiela on 2015-03-16.
 */
public class RWLock {

    private int readers = 0;
    private int writers = 0;
    private int writeRequests = 0;

    private int writesGranted = 0;
    private int readsGranted = 0;

    public synchronized void acquireWriteLock() throws InterruptedException {
        writeRequests++;
        while(writers > 0 || readers > 0) {
            wait();
        }
        writeRequests--;
        writers++;
        writesGranted++;
    }

    public synchronized void releaseWriteLock() {
        writers--;
        notifyAll();
    }

    public synchronized void acquireReadLock() throws InterruptedException {
        while(writers > 0 || writeRequests > 0){
            wait();
        }
        readers++;
        readsGranted++;
    }

    public synchronized void releaseReadLock() {
        readers--;
        notifyAll();
    }

    public synchronized void printStats() {
        System.out.println();
        System.out.println("Writes Granted: " + writesGranted);
        System.out.println("Reads Granted: " + readsGranted);
        System.out.println();
    }
}
