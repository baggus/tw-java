package lab2;

/**
 * Created by Aleksander Ciepiela on 2015-03-16.
 */
public class Box {

    private int mValue;

    public void store(int value) throws InterruptedException {
        Thread.sleep(100);
        mValue = value;
    }

    public int retrieve() throws InterruptedException {
        Thread.sleep(100);
        return mValue;
    }
}
