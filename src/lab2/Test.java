package lab2;

import java.util.Random;

/**
 * Created by Aleksander Ciepiela on 2015-03-16.
 */
public class Test {

    public static void main(String[] args) {

        final Box box = new Box();
        final RWLock lock = new RWLock();

        for(int i = 0; i < 2; i++) {
            new WriteThread(box, lock).start();
        }

        for(int i = 0; i < 10; i++) {
            new ReadThread(box, lock).start();
        }

        new Thread(() -> {
            while(true) {
                lock.printStats();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private static class ReadThread extends Thread {

        private Box mBox;
        private RWLock mLock;

        public ReadThread(Box box, RWLock lock) {
            mBox = box;
            mLock = lock;
        }

        @Override
        public void run() {
            final Random r = new Random();
            while (true) {
                try {
                    mLock.acquireReadLock();
                    System.out.println(getName() + " starts reading");
                    int value  = mBox.retrieve();
                    System.out.println(getName() + " read " + value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    mLock.releaseReadLock();
                }
                try {
                    Thread.sleep(r.nextInt(1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static class WriteThread extends Thread {

        private Box mBox;
        private RWLock mLock;

        public WriteThread(Box box, RWLock lock) {
            mBox = box;
            mLock = lock;
        }

        @Override
        public void run() {
            final Random r = new Random();
            while (true) {
                try {
                    mLock.acquireWriteLock();
                    System.out.println(getName() + " starts writing");
                    final int value = r.nextInt() % 100;
                    mBox.store(value);
                   System.out.println(getName() + " wrote " + value);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    mLock.releaseWriteLock();
                }
                try {
                    Thread.sleep(r.nextInt(1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
