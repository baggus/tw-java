package lab7;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.atomic.AtomicLong;

import static java.nio.file.FileVisitResult.CONTINUE;

/**
 * Created by Aleksander Ciepiela on 2015-05-07.
 */
public class Main {


    public static void main(String[] args) throws IOException, InterruptedException {

        long start, stop;
        String path = "C:\\Users\\Aleksander\\Desktop\\gill-PAM08";


        for (int i = 0; i < 5; i++) {
            start = System.currentTimeMillis();
            asynchronous(path);
            stop = System.currentTimeMillis();
            System.out.println("Asynchronous took: " + (stop - start));
            start = System.currentTimeMillis();
            synchronous(path);
            stop = System.currentTimeMillis();
            System.out.println("Synchronous took: " + (stop - start));
        }

    }

    private static void synchronous(String path) throws IOException {
        SynchronousWalker walker = new SynchronousWalker();
        Files.walkFileTree(Paths.get(path), walker);
        System.out.println("Total number of lines: " + walker.totalNumberOfLines);
    }

    private static void asynchronous(String path) throws IOException, InterruptedException {
        AsynchronousWalker walker = new AsynchronousWalker();
        Files.walkFileTree(Paths.get(path), walker);
        System.out.println("Total number of lines: " + walker.totalNumberOfLines);
    }

    private static class SynchronousWalker extends SimpleFileVisitor<Path> {

        private long totalNumberOfLines = 0;

        @Override
        public FileVisitResult visitFile(Path file,
                                         BasicFileAttributes attr) {
            if (attr.isRegularFile()) {
                try {
                    RandomAccessFile aFile = new RandomAccessFile(file.toFile(), "r");
                    FileChannel inChannel = aFile.getChannel();
                    ByteBuffer buf = ByteBuffer.allocate(1000);

                    int bytesRead = inChannel.read(buf);
                    while (bytesRead != -1) {
                        buf.flip();
                        while (buf.hasRemaining()) {
                            if ((char) buf.get() == '\n') {
                                totalNumberOfLines++;
                            }
                        }
                        buf.clear();
                        bytesRead = inChannel.read(buf);
                    }
                    aFile.close();
                } catch (IOException e) {
                    System.out.println("cannot read file: " + file);
                }
            }
            return CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir,
                                                  IOException exc) {
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file,
                                               IOException exc) {
            System.err.println(exc);
            return CONTINUE;
        }
    }

    private static class AsynchronousWalker extends SimpleFileVisitor<Path> implements CompletionHandler<Integer, ByteBuffer> {

        private AtomicLong totalNumberOfLines = new AtomicLong(0);


        @Override
        public FileVisitResult visitFile(Path file,
                                         BasicFileAttributes attr) throws IOException {
            if (attr.isRegularFile()) {
                AsynchronousFileChannel fileChannel = AsynchronousFileChannel.open(file, StandardOpenOption.READ);
                ByteBuffer buffer = ByteBuffer.allocate(20000);
                fileChannel.read(buffer, 0, buffer, this);
            }
            return CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir,
                                                  IOException exc) {
            return CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file,
                                               IOException exc) {
            System.err.println(exc);
            return CONTINUE;
        }

        @Override
        public void completed(Integer result, ByteBuffer attachment) {
            attachment.flip();
            byte[] data = new byte[attachment.limit()];
            attachment.get(data);
            long count = new String(data).chars().filter(i -> i == '\n').count();
            totalNumberOfLines.addAndGet(count);
            attachment.clear();
        }

        @Override
        public void failed(Throwable exc, ByteBuffer attachment) {
            System.err.println(exc);
        }
    }


}
