package lab1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Aleksander Ciepiela on 2015-03-09.
 */
public class Semaphore {

    private final AtomicInteger counter = new AtomicInteger(1);


    public void acquire() throws InterruptedException {

        while(!counter.compareAndSet(1,0)) {
            Thread.yield();
        }

    }

    public void release() {
        counter.getAndIncrement();
    }


}
