package lab1;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Aleksander Ciepiela on 2015-03-09.
 */
public class Main {

    private int aIntegerForSemaphore;
    private int aInteger;
    private AtomicInteger aa = new AtomicInteger(0);
    private static Semaphore semaphore = new Semaphore();

    private void doSthWithSemaphore() {

        final int N = 1_000_000;

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                try {
                    semaphore.acquire();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                aIntegerForSemaphore--;
                semaphore.release();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                aIntegerForSemaphore++;
                semaphore.release();
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(aIntegerForSemaphore);

    }

    private void doSth() {

        final int N = 1_000_000;

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                aInteger--;
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                aInteger++;
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(aInteger);

    }

    private void doSthWithAtomicInteger() {

        final int N = 1_000_000;

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                aa.decrementAndGet();
            }
        });

        Thread t2 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                aa.incrementAndGet();
            }
        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(aa);

    }




    public static void main(String... args) {


        Main main = new Main();

        long start, end;

        //without locking
        start = System.currentTimeMillis();
        main.doSth();
        end = System.currentTimeMillis();
        System.out.println(end - start);

        System.out.println();

        //Atomic integer
        start = System.currentTimeMillis();
        main.doSthWithAtomicInteger();
        end = System.currentTimeMillis();
        System.out.println(end - start);

        System.out.println();

        //semaphore
        start = System.currentTimeMillis();
        main.doSthWithSemaphore();
        end = System.currentTimeMillis();
        System.out.println(end - start);

    }
}
